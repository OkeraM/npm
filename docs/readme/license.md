## License

Copyright © {{ copyright }} [{{ company_name }}]({{ website.homepage }}). This project is [{{ license }}]({{ repository.group.npm }}/{{ slug }}/-/raw/master/LICENSE) licensed.
