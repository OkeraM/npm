<div align="center">
  <center>
    <a href="{{ repository.group.npm }}/{{ slug }}" title="{{ pkg.name }} GitLab page" target="_blank">
      <img width="100" height="100" alt="{{ pkg.name }} logo" src="{{ repository.group.npm }}/{{ slug }}/-/raw/master/logo.png" />
    </a>
  </center>
</div>
<div align="center">
  <center><h1 align="center">NPM Package: {{ project_title }}</h1></center>
</div>
