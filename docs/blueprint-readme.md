{{ load:.modules/docs/readme/header.md }}
{{ load:.modules/docs/readme/subheader.md }}
{{ load:.modules/docs/readme/quick-description.md }}
{{ template:toc }}
{{ load:.modules/docs/readme/requirements.md }}
{{ load:.blueprint.md }}
{{ load:.modules/docs/readme/contributing-details.md }}
{{ load:.modules/docs/readme/license.md }}
