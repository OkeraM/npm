## Contributing

Contributions, issues, and feature requests are welcome! Feel free to check the [issues page]({{ repository.github }}{{ repository.location.issues.github }}). If you would like to contribute, please take a look at the [contributing guide]({{ repository.github }}{{ repository.location.contributing.github }}).

<details>
<summary><b>Sponsorship</b></summary>
<br/>
<blockquote>
<br/>
Dear Lovely Person,<br/><br/>
{{ sponsorship.text }}
<br/><br/>Sincerely,<br/><br/>

**_{{ sponsorship.author }}_**<br/><br/>

</blockquote>

<a href="{{ profile.patreon }}">
  <img src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" width="160">
</a>

</details>
