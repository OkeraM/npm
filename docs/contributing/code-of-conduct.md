## Code of Conduct

This project and everyone participating in it is governed by the [Code of Conduct]({{ repository.group.npm }}/{{ slug }}/-/blob/master/CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [{{ contact_email }}](mailto:{{ contact_email }}).
